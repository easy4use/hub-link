/**
 * Project: yui3-common-tools
 * Class DateUtils
 * Version 1.0
 * File Created at 2020年8月3日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * 日期处理类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class DateUtils {

    final static public String FULL_ST_FORMAT = "yyyy-MM-dd HH:mm:ss";
    final static public String ST_FORMAT = "yyyy-MM-dd HH:mm";
    final static public String DAY_FORMAT = "yyyy-MM-dd";
    final static public String CURRENCY_J_FORMAT = "yyyy/MM/dd HH:mm";
    final static public String DATA_FORMAT = "yyyyMMddHHmmss";
    final static public String CURRENCY_J_DATA_FORMAT = "yyyy/MM/dd";

    public static String format() {
        return format(FULL_ST_FORMAT);
    }
    
    public static String format(String pattern) {
        return format(new Date(), pattern);
    }
    
    public static String format(Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(FULL_ST_FORMAT);
        return dateFormat.format(date);
    }
    
    public static String format(Date date, String pattern) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(date);
    }
    
    public static Date formatDate(String dateStr, String pattern) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date = format.parse(dateStr);
            return date;
        } catch (ParseException e) {}
        return null;
    }
    
    public static java.sql.Date formatSqlDate(String dateStr, String pattern) {
        Date date = formatDate(dateStr, pattern);
        if (null != date) {
            return new java.sql.Date(date.getTime());
        }
        return null;
    }
    
    public static Timestamp formatTimestamp(String dateStr, String pattern) {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            Date date = format.parse(dateStr);
            return new Timestamp(date.getTime());
        } catch (ParseException e) {
        }
        return null;
    }
}
