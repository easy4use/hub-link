package yui.comn.hub.utils;

/**
 * Project: yui3-common-tools
 * Class StringUtils
 * Version 1.0
 * File Created at 2020年8月3日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * String 操作工具类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class StrUtils {

    /**
     * string 首字母小写
     */
    public static String lowerCaseFirstChar(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        
        if(Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        
        return new StringBuilder().append(Character.toLowerCase(str.charAt(0)))
                .append(str.substring(1)).toString();
    }
    
    /**
     * string 首字母大写
     */
    public static String upperCaseFirstChar(String str) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        
        if(Character.isUpperCase(str.charAt(0))) {
            return str;
        }
        
        return new StringBuilder().append(Character.toUpperCase(str.charAt(0)))
                .append(str.substring(1)).toString();
    }
    
    /**
     * string加前缀  例如 SysUser  t_  -->   t_Sys_User
     */
    public static String upperCaseFirstCharAndAddPrefix(String str, String prefix) {
        return prefix + upperCaseFirstChar(str);
    }
    
    /**
     * 驼峰形式变下划线  sysUser  -->   SYS_USER
     */
    public static String toUnderlineAndUpperCaseByHump(String humpStr) {
        if (StringUtils.isBlank(humpStr)) {
            return humpStr;
        }
        
        StringBuffer result = new StringBuffer();
        // 将第一个字符处理成大写
        result.append(humpStr.substring(0, 1).toUpperCase());
        // 循环处理其余字符
        for (int i = 1; i < humpStr.length(); i++) {
            String s = humpStr.substring(i, i + 1);
            // 在大写字母前添加下划线
            if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
                result.append("_");
            }
            // 其他字符直接转成大写
            result.append(s.toUpperCase());
        }
        return result.toString();
    }
    
    /**
     * 驼峰形式变下划线  sysUser  -->   sys_user
     */
    public static String toUnderlineAndLowerCaseByHump(String humpStr) {
        if (StringUtils.isBlank(humpStr)) {
            return humpStr;
        }
        StringBuffer result = new StringBuffer();
        // 将第一个字符处理成大写
        result.append(humpStr.substring(0, 1).toLowerCase());
        // 循环处理其余字符
        for (int i = 1; i < humpStr.length(); i++) {
            String s = humpStr.substring(i, i + 1);
            // 在大写字母前添加下划线
            if (s.equals(s.toUpperCase()) && !Character.isDigit(s.charAt(0))) {
                result.append("_");
            }
            // 其他字符直接转成大写
            result.append(s.toLowerCase());
        }
        return result.toString();
    }
}
