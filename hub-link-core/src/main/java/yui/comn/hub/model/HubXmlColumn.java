/**
* Project: yui3-common-hub
 * Class HubXmlColumn
 * Version 1.0
 * File Created at 2018年8月10日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Data
public class HubXmlColumn implements Comparable <HubXmlColumn> {

    private String prefix;      //name的前缀, 比如prefix=sysUser, name=nm, 输出就是 sysUserNm
    private String name;        //grid: map中的key; search: 查询的key
    private String mapper;      //grid: 对象.属性; search: 表名+字段 
    private String column;      //grid: 表名+字段 
    private String type;        //通过解析对象属性，反射生成列
    private String typeObj;     //实体对象名称
    private String descr;       //列描述 
    private Integer viewOrder;  //列排序，如果写，默认按顺序排序，如果解析类对象，按照英文首字母排序
    private String handler;     //处理器，多个处理器用分号(";")隔开
    private String toName;      //如果通过处理器想生成另一个map属性
    private String toDescr;     //是新属性列排序描述
    private String enCls;       //枚举类
    private String group;       //组的概念

    private Class<?> enumClass; //枚举实例化类
    
    private List<HubXmlHandlerConfig> handlerConfigs;
    
    private Map<String, List<HubXmlColumn>> collections;
    
    public void putCollection(String key, List<HubXmlColumn> collection) {
        if (null == collections) {
            collections = new LinkedHashMap<>();
        }
        collections.put(key, collection);
    }
    
    public HubXmlColumn() {}
    
    public HubXmlColumn(String name, String mapper) {
        this.name = name;
        this.mapper = mapper;
    }
    
    public HubXmlColumn addHandler(String handler) {
        if (StringUtils.isBlank(handler)) {
            this.handler = handler;
        } else {
            this.handler = this.handler + Symbol.SEMICOLON + handler;
        }
        return this;
    }
    
    @Override
    public int compareTo(HubXmlColumn obj) {
        if (obj.getViewOrder() == null) {
            return 1;
        }
        
        if (this.getViewOrder() == null) {
            return -1;
        } 

        return this.getViewOrder().compareTo(((HubXmlColumn) obj).getViewOrder());
    }
    
}
