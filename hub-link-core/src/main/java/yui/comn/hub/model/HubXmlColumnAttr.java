/**
* Project: yui3-common-hub
 * Class HubXmlColumnAttr
 * Version 1.0
 * File Created at 2018年8月10日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.model;

/**
 * xml column中元素属性
 * @author yuyi (1060771195@qq.com)
 */
public enum HubXmlColumnAttr {
    prefix,       //name的前缀, 比如prefix=sysUser, name=nm, 输出就是 sysUserNm
    name,         //grid: map中的key; search: 查询的key
    mapper,       //grid: 对象.属性; search: 表名+字段 
    column,       //grid: 表名+字段 
    type,         //通过解析对象属性，反射生成列
    typeObj,      //实体对象名称
    descr,        //列描述 
    viewOrder,    //列排序，如果写，默认按顺序排序，如果解析类对象，按照英文首字母排序
    handler,      //处理器，多个处理器用分号(";")隔开
    toName,       //如果通过处理器想生成另一个map属性
    toDescr,      //如果通过处理器想生成另一个map属性描述
    enCls,        //枚举类
    group,        //组的概念
    ;
    
}
