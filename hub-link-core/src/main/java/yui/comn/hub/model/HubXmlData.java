package yui.comn.hub.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class HubXmlData implements Serializable {
	private static final long serialVersionUID = 2617901546811593713L;
	
	private String name;        //grid: map中的key; search: 查询的key
    private String mapper;      //grid: 对象.属性; search: 表名+字段 
    private String descr;       //列描述 
    private Object value;       //原始值 
    private Object toValue;     //handle处理后的值
    
}
