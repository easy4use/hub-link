/**
* Project: yui3-common-hub
 * Class HubDataHandlerRegister
 * Version 1.0
 * File Created at 2018年8月14日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.handler;

import java.util.HashMap;
import java.util.Map;

/**
 * 抽象数据中转处理注册类
 * @author yuyi (1060771195@qq.com)
 */ 
public class HubDataHandlerRegister {
    
    private static Map<String, IHubDataHandler> handlers = new HashMap<>();

    /*//公共handler注册器建议使用显性注入注册方式
    public HubDataHandlerRegister() {
        // handlers.put("test", new TestHubDataHandler());
        // handlers.put("trans", new TransHubDataHandler());
        // handlers.put("jsonFormat", new JsonFormatHubDataHandler());
        // handlers.put(EnumClassHubDataHandler.REG_NAME, new EnumClassHubDataHandler());
    }*/
    
    public static void registerHandler(String name, IHubDataHandler handler) {
        if (handlers.containsKey(name)) {
            throw new RuntimeException("xml数据处理器已经存在, name=" + name + ", handler=" + handlers.get(name));
        }
        handlers.put(name, handler);
    }
    
    public static IHubDataHandler getHandler(String key) {
        return handlers.get(key);
    }

}
