/**
* Project: yui3-common-hub
 * Class IHubDataHandler
 * Version 1.0
 * File Created at 2018年8月14日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.handler;

import java.util.Map;

import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * 抽象数据中转处理
 * @author yuyi (1060771195@qq.com)
 */
public interface IHubDataHandler {
    
    Object handle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap);

}
