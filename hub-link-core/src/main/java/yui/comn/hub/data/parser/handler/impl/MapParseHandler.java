/**
 * Project: yui3-common-hub-core
 * Class MapParseHandler
 * Version 1.0
 * File Created at 2020-12-17
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.parser.handler.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import yui.comn.hub.data.parser.handler.AbstractHubDataParseHandler;

/**
 * <p>
 * jackson map
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class MapParseHandler extends AbstractHubDataParseHandler {

    public static final String REG_NAME = "map";

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }
    
    @Override
    public Object dataParse(Object obj) {
        if (obj instanceof Map) {
            return obj;
        }
        try {
            return getObjectMapper().convertValue(obj, Map.class);
        } catch (Exception e) {
            String errMsg = "convertValue error";
            log.error(errMsg, e);
            throw new RuntimeException(errMsg, e);
        }
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Object getObjByAttr(Object obj, String attrName) {
        return ((Map) obj).get(attrName);
    }

    @SuppressWarnings("rawtypes")
    @Override
    protected Object getAttrValue(Object obj, String attrName) {
        return ((Map) obj).get(attrName);
    }

}
