/**
* Project: yui3-common-hub
 * Class HubDataToMapParser
 * Version 1.0
 * File Created at 2018年8月17日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.data.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import yui.comn.hub.data.handler.HubDataHandlerProcess;
import yui.comn.hub.data.parser.handler.HubDataParseRegister;
import yui.comn.hub.data.parser.handler.impl.MapParseHandler;
import yui.comn.hub.model.HubXmlColumn;
import yui.comn.hub.xml.parser.HubXmlCache;

/**
 * 把Dto对象解析成map对象
 * @author yuyi (1060771195@qq.com)
 */
public class HubDataToMapParser {
    // private Logger logger = LoggerFactory.getLogger(HubDataToMapParser.class);
    
    private String parserName = MapParseHandler.REG_NAME;
    
    private static HubDataToMapParser parser = new HubDataToMapParser();
    
    public static HubDataToMapParser getInstance() {
        return parser;
    }
    
    public void setParserName(String parserName) {
        this.parserName = parserName;
    }
    
    /**
     * 对象转Map
     */
    public Map<String, Object> toMapByObj(Class<?> clazz, String name, Object obj) {
        if (obj == null) {
            return null;
        }
        List<HubXmlColumn> colList = HubXmlCache.getInstance().toGridList(clazz, name);
        return toMapByObj(colList, obj);
    }
    
    /**
     * 列表转Map列表
     */
    public List<Map<String, Object>> toMapListByObjList(Class<?> clazz, String name, Collection<?> list) {
    	List<HubXmlColumn> colList = HubXmlCache.getInstance().toGridList(clazz, name);
        return toMapListByObjList(colList, list);
    }
    
    
    /**
     * 列表转Map列表
     */
    private List<Map<String, Object>> toMapListByObjList(List<HubXmlColumn> colList, Object object) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        
        if (object instanceof ArrayNode) {
            ArrayNode arrayNode = (ArrayNode) object;
            for (JsonNode jsonNode : arrayNode) {
                resultList.add(toMapByObj(colList, jsonNode));
            }
        } else if (object instanceof Collection) {
            Collection<?> list = (Collection<?>) object;
            if (!CollectionUtils.isEmpty(list)) {
                for (Object obj : list) {
                    resultList.add(toMapByObj(colList, obj));
                }
            }
        }
        return resultList;
    }
    
    
    /**
     * 对象转Map
     */
    private Map<String, Object> toMapByObj(List<HubXmlColumn> colList, Object obj) {
        obj = HubDataParseRegister.getHandler(parserName).dataParse(obj);
        
        Map<String, Object> resultMap = new LinkedHashMap<>();
        
        List<HubXmlColumn> columnHandlers = new ArrayList<>();
        for (HubXmlColumn column : colList) {
            if (null == column.getCollections()) {
                if (StringUtils.isBlank(column.getMapper())) {
                    continue;
                }
                if (StringUtils.isNotBlank(column.getGroup())) {
                    Map<String, Object> groupMap = (Map<String, Object>) resultMap.get(column.getGroup());
                    if (null == groupMap) {
                        groupMap = new LinkedHashMap<>();
                        resultMap.put(column.getGroup(), groupMap);
                    }
                    groupMap.put(column.getName(), getAttrValue(obj, column.getMapper()));
                } else {
                    resultMap.put(column.getName(), getAttrValue(obj, column.getMapper()));
                }
                if (!CollectionUtils.isEmpty(column.getHandlerConfigs())) {
                    columnHandlers.add(column);
                }
            } else {
                Map<String, List<HubXmlColumn>> collections = column.getCollections();
                for (Entry<String, List<HubXmlColumn>> entry : collections.entrySet()) {
                    List<Map<String, Object>> mapList = toMapListByObjList(entry.getValue(), getAttrValue(obj, column.getMapper()));
                    resultMap.put(column.getName(), mapList);
                }
            }
            
        }
        //数据处理器
        for (HubXmlColumn hubXmlColumn : columnHandlers) {
            HubDataHandlerProcess.getInstance().handle(hubXmlColumn, resultMap);
        }
        
        return resultMap;
    }
    /***************************************************dto 转   xml***************************************************/
    /**
     * 例如：sysUserDtox.sysUserVo.sysUserPk
     */
    private Object getAttrValue(Object obj, String mapper) {
        return HubDataParseRegister.getHandler(parserName).get(obj, mapper);
    }
    
}
