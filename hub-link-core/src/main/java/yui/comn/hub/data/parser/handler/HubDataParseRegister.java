/**
 * Project: yui3-common-hub-core
 * Class HubDataParseRegister
 * Version 1.0
 * File Created at 2020-12-17
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.parser.handler;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 数据解析器注册类
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public class HubDataParseRegister {
    private static Map<String, IHubDataParseHandler> handlers = new HashMap<>();
    
    public static void registerHandler(String name, IHubDataParseHandler handler) {
        if (handlers.containsKey(name)) {
            throw new RuntimeException("xml数据处理器已经存在, name=" + name + ", handler=" + handlers.get(name));
        }
        handlers.put(name, handler);
    }
    
    public static IHubDataParseHandler getHandler(String key) {
        return handlers.get(key);
    }
}
