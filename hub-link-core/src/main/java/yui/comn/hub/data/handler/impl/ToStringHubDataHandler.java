/**
 * Project: yui3-common-hub-core
 * Class ToStringHubDataHandler
 * Version 1.0
 * File Created at 2020-12-10
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.data.handler.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import yui.comn.hub.data.handler.AbstractHubDataHandler;
import yui.comn.hub.model.HubXmlHandlerConfig;

/**
 * <p>
 * 转化为String
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
@Service
public class ToStringHubDataHandler extends AbstractHubDataHandler {

    public static final String REG_NAME = "toStr";
    
    @Override
    protected void doHandle(HubXmlHandlerConfig dataConfig, Map<String, Object> dataMap) {
        Object obj = dataMap.get(dataConfig.getName());
        if (null != obj) {
            dataMap.put(dataConfig.getName(), String.valueOf(obj));
        }
    }

    @Override
    protected String getRegisterName() {
        return REG_NAME;
    }

    
}
