/**
* Project: yui3-common-hub
 * Class HubConfig
 * Version 1.0
 * File Created at 2018年8月18日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import yui.comn.hub.data.parser.HubDataToMapParser;
import yui.comn.hub.xml.parser.HubXmlCache;
import yui.comn.hub.xml.plugins.HubXmlPlugin;

/**
 * @author yuyi (1060771195@qq.com)
 */
@Configuration
@EnableConfigurationProperties(HubProperties.class)
public class HubSysConfig {

    @Autowired(required = false)
    private HubXmlPlugin hubXmlPlugin;
    
    @Bean("hubXmlCache")
    public HubXmlCache hubXmlCache(HubProperties hubProperties) {
        if (null != hubProperties.getCache()) {
            HubXmlCache.getInstance().setCache(hubProperties.getCache());
        }
        HubXmlCache.getInstance().initHubXmlParser(hubXmlPlugin);
        return HubXmlCache.getInstance();
    }
    
    @Bean("hubDataToMapParser")
    public HubDataToMapParser hubDataToMapParser(HubProperties hubProperties) {
        HubDataToMapParser.getInstance().setParserName(hubProperties.getParser());
        return HubDataToMapParser.getInstance();
    }
    
}
