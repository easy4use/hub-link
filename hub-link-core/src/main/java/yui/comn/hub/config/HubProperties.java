/**
* Project: yui3-common-hub
 * Class HubProperties
 * Version 1.0
 * File Created at 2018年8月18日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import yui.comn.hub.data.parser.handler.impl.MapParseHandler;

/**
 * hub 配置属性
 * @author yuyi (1060771195@qq.com)
 */
@Data
@ConfigurationProperties(HubProperties.targetName)
public class HubProperties {
    public static final String targetName = "hub-link.core";
    
    //HubXmlCache是否开启缓存，开发阶段建议关闭缓存，默认是开启
    private Boolean cache;
    
    private String parser = MapParseHandler.REG_NAME;
    
}
