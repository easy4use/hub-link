/**
 * Project: yui3-common-hub-core
 * Class HubXmlPlugin
 * Version 1.0
 * File Created at 2020-12-14
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.xml.plugins;

import java.lang.annotation.Annotation;

import yui.comn.hub.model.HubXmlColumn;

/**
 * <p>
 * xml 处理插件
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 */
public interface HubXmlPlugin {

    void xmlHandle(Annotation[] annos, HubXmlColumn col);
    
}
