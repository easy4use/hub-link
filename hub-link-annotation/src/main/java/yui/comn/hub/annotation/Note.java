/**
* Project: yui3-common-utils
 * Class Note
 * Version 1.0
 * File Created at 2018年8月11日
 * $Id$
 * 
 * Copyright 2010-2015 Yui.com Corporation Limited.
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * Yui Personal. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Yui.com.
 */
package yui.comn.hub.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.core.annotation.AliasFor;

import yui.comn.hub.model.Clazz;

/**
 * 对象属性注释
 * @author yuyi (1060771195@qq.com)
 */
@Target({ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Note {

    /**
     * 默认注释
     * @return
     */
    @AliasFor("descr")
    String value() default "";
    
    /**
     * 默认注释
     * @return
     */
    @AliasFor("value")
    String descr() default "";
    
    /**
     * 属性排序
     * @return
     */
    int viewOrder() default 50;  
    
    /**
     * 处理方式
     * @return
     */
    String handler() default "";  
    
    /**
     * 生成一个新的元素（不覆盖原来）, 该属性只有在handler存在时才有效
     * @return
     */
    String toName() default "";  
    
    /**
     * 生成一个新的元素的描述（不覆盖原来）, 该属性只有在handler存在时才有效
     * @return
     */
    String toDescr() default "";  
    
    /**
     * 枚举类
     * @return
     */
    Class<?> enCls() default Clazz.class;  
    
}
