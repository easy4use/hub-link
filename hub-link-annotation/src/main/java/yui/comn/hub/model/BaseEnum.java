/**
 * Project: yui3-common-api
 * Class BaseEnum
 * Version 1.0
 * File Created at 2020年8月1日
 * $Id$
 * author yuyi
 * email 1060771195@qq.com
 */
package yui.comn.hub.model;

/**
 * <p>
 * 基础枚举类接口
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @param <T>
 */
public interface BaseEnum<T> {

    T cd();
    
    String nm();
}
