package yui.comn.hub.model;

/**
 * 加解密类型
 * @author yuyi (1060771195@qq.com)
 */

public enum CryptType {

	/**
	 * 空
	 */
	NULL(""),
	/**
	 * 整个
	 */
    ALL("all"),
    
    /**
     * 部分
     */
    PART("part");
    
    private String key;

	CryptType(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
    
	public static CryptType get(String key) {
		for (CryptType itm : CryptType.values()) {
			if (itm.getKey().equals(key)) {
				return itm;
			}
		}
		return null;
	}
	
}
