package yui.comn.hub.model;

/**
 * 加解密模式
 * @author yuyi (1060771195@qq.com)
 */

public enum CryptMode {

	/**
	 * 空
	 */
	NULL(""),
	/**
	 * aes
	 */
    AES("aes"),
    
    /**
     * base64
     */
    BASE64("base64");
    
    private String key;

	CryptMode(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
    
	public static CryptMode get(String key) {
		for (CryptMode itm : CryptMode.values()) {
			if (itm.getKey().equals(key)) {
				return itm;
			}
		}
		return null;
	}
	
}
