package yui.bss.test.mgr;

import yui.bss.test.dto.TestBDto;
import yui.bss.test.vo.TestBVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yui
 */
public interface TestBMgr extends IMgr<TestBVo, TestBDto> {

    
}
