package yui.bss.test.mgr;

import yui.bss.test.dto.TestDDto;
import yui.bss.test.vo.TestDVo;
import yui.comn.mybatisx.extension.mgr.IMgr;

/**
 * <p>
 * 测试E
 * </p>
 *
 * @author yui
 */
public interface TestDMgr extends IMgr<TestDVo, TestDDto> {

}
