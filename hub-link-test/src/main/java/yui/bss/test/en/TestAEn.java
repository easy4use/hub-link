package yui.bss.test.en;

import yui.comn.hub.model.BaseEnum;

import java.io.Serializable;

/**
 * 作品 En
 *
 * @author yui
 * @since 2024-08-14
 */
public class TestAEn implements Serializable {
    private static final long serialVersionUID = 1L;

    public enum Type implements BaseEnum<Integer> {
        ZORE(0, "中文"),
        ONE(1, "英文"),
        TWO(2, "法语"),
        THREE(3, "德语"),
        ;

        private Integer cd; private String nm;
        Type (Integer cd, String nm) {
            this.cd = cd;
            this.nm = nm;
        }

        public Integer cd() {
            return cd;
        }

        public String nm() {
            return nm;
        }
    }


}