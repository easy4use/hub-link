package yui.bss.test.dto;

import java.io.Serializable;

import lombok.Data;
import yui.bss.test.vo.TestCVo;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yui
 */
@Data
public class TestCDto implements Serializable {
    private static final long serialVersionUID = 1L;

    protected TestCVo testCVo;

}
