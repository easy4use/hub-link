package yui.bss.test.dao;

import org.apache.ibatis.annotations.Mapper;
import yui.bss.test.dto.TestCDto;
import yui.bss.test.vo.TestCVo;
import yui.comn.mybatisx.core.mapper.BaseDao;

/**
 * <p>
 * 测试C Mapper 接口
 * </p>
 *
 * @author yui
 */
@Mapper
public interface TestCDao extends BaseDao<TestCVo, TestCDto> {

}
