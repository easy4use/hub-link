package yui.bss.test.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import yui.bss.test.en.TestAEn;
import yui.comn.hub.annotation.Note;

/**
 * <p>
 * 测试C
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_test_c")
public class TestCVo extends BaseVo {
    private static final long serialVersionUID = 1L;

    @Note("ID")
    private Long id;

    @Note("aID")
    private Long aid;
    
    @Note("bID")
    private Long bid;

    @Note("cID")
    private Long cid;

    @Note(value = "type", enCls = TestAEn.Type.class)
    private Integer type;

}
