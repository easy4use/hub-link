package yui.bss.test.vo;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;

import lombok.Data;
import yui.comn.hub.annotation.Note;

/**
 * <p>
 * 基础实体类
 * </p>
 * @author yuyi (1060771195@qq.com)
 */
@Data
public abstract class BaseVo implements Serializable {
    private static final long serialVersionUID = 9218209445883907541L;

    @Note(value = "创建时间", viewOrder = 101)
    @TableField(fill = FieldFill.INSERT)
    private Date crtTm;

    @Note(value = "创建人", viewOrder = 102)
    @TableField(fill = FieldFill.INSERT)
    private String crtBy;

    @Note(value = "修改时间", viewOrder = 103)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    // @JsonFormat(pattern = DateUtils.FULL_ST_FORMAT)
    private Date updTm;

    @Note(value = "修改人", viewOrder = 104)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updBy;

    @TableLogic
    private Integer editFlag;
}
