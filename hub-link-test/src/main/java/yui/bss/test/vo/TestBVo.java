package yui.bss.test.vo;

import com.baomidou.mybatisplus.annotation.TableName;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import yui.bss.test.en.TestAEn;
import yui.comn.hub.annotation.Note;

/**
 * <p>
 * 测试B
 * </p>
 *
 * @author yuyi (1060771195@qq.com)
 * @since 2019-03-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_test_b")
public class TestBVo extends BaseVo {
    private static final long serialVersionUID = 1L;

    @Note("id")
    private Long id;

    @Note("aid")
    private Long aid;

    @Note("cid")
    private Long cid;

    @Note(value = "type", enCls = TestAEn.Type.class)
    private Integer type;

}
