package yui.comn.hub.extension.utils;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

public class CryptBase64Utils {
	public static final Base64 base64 = new Base64();
	
	public static String decrypt(String text) {
		try {
			return new String(base64.decode(text), "UTF-8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
	
	public static String encrypt(String text) {
		try {
			byte[] textByte = text.getBytes("UTF-8");
			return base64.encodeToString(textByte);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return text;
	}
//	//编码
//	final String encodedText = base64.encodeToString(textByte);
//	System.out.println(encodedText);
//	//解码
//	System.out.println(new String(base64.decode(encodedText), "UTF-8"));
//
//	final Base64 base64 = new Base64();
//	final String text = "字串文字";
//	final byte[] textByte = text.getBytes("UTF-8");
//	//编码
//	final String encodedText = base64.encodeToString(textByte);
//	System.out.println(encodedText);
//	//解码
//	System.out.println(new String(base64.decode(encodedText), "UTF-8"));
}
