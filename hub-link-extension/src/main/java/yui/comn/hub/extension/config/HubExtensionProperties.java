package yui.comn.hub.extension.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;
import lombok.experimental.Accessors;
import yui.comn.hub.model.CryptMode;
import yui.comn.hub.model.CryptType;

@Data
@Accessors(chain = true)
@ConfigurationProperties(prefix = "hub-link.extension")
public class HubExtensionProperties {

	private boolean openHeader = true;
	
	private int titleRowNum = 0;
	
	private int bodyRowNum = 1;
	
	private String cryptType = CryptType.PART.getKey();
	
	private String cryptMode = CryptMode.AES.getKey();
	
	private boolean crypt = true;
	
	private boolean decrypt = false;
	
	private boolean encrypt = false;
	
	private String redisPrefix = "yui3:enum";
	
	private String typeEnumsPackage = "yui.bss.*.en, yui.comn.*.en";
	
}
