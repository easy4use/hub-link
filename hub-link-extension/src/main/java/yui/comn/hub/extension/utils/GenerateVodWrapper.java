package yui.comn.hub.extension.utils;

import java.util.List;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;

import yui.comn.hub.extension.model.VodWrapper;
import yui.comn.hub.utils.StrUtils;
import yui.comn.mybatisx.extension.conditions.SqlCompareMode;
import yui.comn.mybatisx.extension.conditions.clauses.OrderClause;
import yui.comn.mybatisx.extension.conditions.clauses.PageClause;
import yui.comn.mybatisx.extension.conditions.clauses.QueryClause;
import yui.comn.mybatisx.extension.conditions.clauses.RuleClause;
import yui.comn.mybatisx.extension.conditions.clauses.WhereClause;

/**
 * <p>
 * 生成查询类
 * </p>
 * @author yuyi (1060771195@qq.com)
 */
public class GenerateVodWrapper {

    public static void wrapperHandle(VodWrapper wrapper, QueryClause queryClause) {
        pageHandle(wrapper, queryClause.getP());
        
        whereHandle(wrapper, queryClause.getW());
        ruleHandle(wrapper, queryClause.getR());
        
        orderHandle(wrapper, queryClause.getO());
    }
    
    public static void pageHandle(VodWrapper wrapper, PageClause pageClause) {
        if (null != pageClause) {
            wrapper.setPageNo(pageClause.getN());
            wrapper.setPageSize(pageClause.getS());
        }
    }
    
    public static void orderHandle(VodWrapper wrapper, List<OrderClause> orderList) {
        if (CollectionUtils.isNotEmpty(orderList)) {
            for (OrderClause orderClause : orderList) {
                if (wrapper.getSortByBuffer().length() > 0) {
                    wrapper.getSortByBuffer().append(",");
                }
                wrapper.getSortByBuffer().append(StrUtils.upperCaseFirstChar(orderClause.getK())).append(":")
                        .append(StrUtils.upperCaseFirstChar(orderClause.getT()));
            }
        } else {
            wrapper.getSortByBuffer().append("CreationTime:Desc");
        }
    }
    
    public static void ruleHandle(VodWrapper wrapper, List<RuleClause> ruleList) {
        if (CollectionUtils.isNotEmpty(ruleList)) {
            for (RuleClause ruleClause : ruleList) {
                whereHandle(wrapper, ruleClause.getW());
            }
        }
    }
    
    public static void whereHandle(VodWrapper wrapper, List<WhereClause> whereList) {
        if (CollectionUtils.isNotEmpty(whereList)) {
            for (WhereClause wc : whereList) {
                if (wrapper.getMatchBuffer().length() > 0) {
                    wrapper.getMatchBuffer().append(" and ");
                }
                
                SqlCompareMode mode = SqlCompareMode.get(wc.getM(), SqlCompareMode.LK);
                
                switch (mode) {
                    case EQ: 
                        wrapper.getMatchBuffer().append(StrUtils.upperCaseFirstChar(wc.getK())).append(" = ").append(wc.getV());
                        break;
                    case NE: 
                        break;
                    case LT: 
                        break;
                    case LE: 
                        break;
                    case GT: 
                        break;
                    case GE:
                        break;
                    case LK: 
                        wrapper.getMatchBuffer().append(StrUtils.upperCaseFirstChar(wc.getK())).append(" = '").append(wc.getV()).append("'");
                        break;
                    case LLK:
                    case RLK:
                        break;
                    case NLK:
                        break;
                    case IN: 
                        wrapper.getMatchBuffer().append(StrUtils.upperCaseFirstChar(wc.getK())).append(" in ('").append(wc.getV()).append("')");
                        break;
                    case NIN:
                        break;
                    case IS: 
                        break;
                    case NIS:
                        break;
                    case BT: 
                        @SuppressWarnings("unchecked")
                        List<String> valArr = (List<String>) wc.getV();
                        String val2 = "'" + valArr.get(0) + "T00:00:00Z','" + valArr.get(1) + "T23:59:59Z'";
                        wrapper.getMatchBuffer().append(StrUtils.upperCaseFirstChar(wc.getK())).append(" = (").append(val2).append(")");
                        break;
                    default:
                        wrapper.getMatchBuffer().append(StrUtils.upperCaseFirstChar(wc.getK())).append(" = '").append(wc.getV()).append("'");
                        break;
                }
            }
        }
    }
    
}
