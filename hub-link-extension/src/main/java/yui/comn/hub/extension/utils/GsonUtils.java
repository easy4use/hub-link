package yui.comn.hub.extension.utils;

import java.util.Map;

import com.google.gson.Gson;

import yui.comn.mybatisx.extension.conditions.clauses.QueryClause;

public class GsonUtils {
	
	private static Gson gson = new Gson();
	
	@SuppressWarnings("rawtypes")
	public static final Class<Map> DEFAULT_TYPE = Map.class;

	
	private GsonUtils () {}
	
	public static <T> T JsonStr2JavaBean(String jsonStr, Class<T> toParseClass){
		if (jsonStr == null) {
			return null;
		}
		return gson.fromJson(jsonStr, toParseClass);
	}
	
	public static void main(String[] args) {
	    String content = "{\"w\":[{\"k\": \"nm\",\"v\": \"123\",\"m\": \"EQ\"},{\"k\": \"nm2\",\"v\": \"1232\",\"m\": \"EQ\"}],\"o\": {\"k\": \"123\",\"t\": \"desc\"}}";
	    QueryClause jsonStr2JavaBean = GsonUtils.JsonStr2JavaBean(content, QueryClause.class);
	    System.out.println(jsonStr2JavaBean);
    }
}
